from num2words import num2words

digit = input("Целое положительное число:")

if 0 <= int(digit) <= 10000000:
    print(num2words(int(digit), to='ordinal', lang='ru'))  # Выведет порядковое числительное
    print(num2words(int(digit), lang='ru'))  # Выведет порядковое числительное
else:
    print("Введите целое число от нуля до 10 миллионов")
