#include <stdio.h>
#include <stdint.h>
//#include <stdlib.h>
#include <time.h>   // for gettimeofday()
#include <unistd.h>     // for sleep()


#define BILLION  1000000000.0

uint32_t A(uint32_t **a, size_t width, size_t h)
{
	uint32_t sum = 0;
	for (size_t i = 0; i < width; i++)
		for (size_t j = 0; j < h; j++)
			sum += a[j][i];
	return sum;
}

uint32_t B(uint32_t **a, size_t w, size_t height)
{
	uint32_t sum = 0;
	for (size_t j = 0; j < height; j++)
		for (size_t i = 0; i < w; i++)
			sum += a[j][i];
	return sum;
}

// основная функция для определения времени выполнения программы на C
int main()
{

    uint32_t a[]={5,6,7,8};
    uint32_t b[]={1,2,3,4};
    uint32_t c[]={5,2,4,8};
    uint32_t *p[]={a,b,c};
    uint32_t **pp=p;

    struct timespec start, end;

    clock_gettime(CLOCK_REALTIME, &start);

    //sleep(1);
    A(p,4,3);
    B(p,4,3);

    clock_gettime(CLOCK_REALTIME, &end);

    // time_spent = конец - начало
    double time_spent = (end.tv_sec - start.tv_sec) +
                        (end.tv_nsec - start.tv_nsec) / BILLION;

    printf("The elapsed time is %.10f seconds", time_spent);

    return 0;
}
